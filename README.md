# TYPO3 Extension `Grid Gallery`

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.me/lavittoag/10)
[![Latest Stable Version](https://poser.pugx.org/lavitto/typo3-gridgallery/v/stable)](https://packagist.org/packages/lavitto/typo3-gridgallery)
[![Total Downloads](https://poser.pugx.org/lavitto/typo3-gridgallery/downloads)](https://packagist.org/packages/lavitto/typo3-gridgallery)
[![License](https://poser.pugx.org/lavitto/typo3-gridgallery/license)](https://packagist.org/packages/lavitto/typo3-gridgallery)

> This extension adds a modern grid gallery content element to your TYPO3 website.

- **Demo**: [www.lavitto.ch/typo3-ext-gridgallery](https://www.lavitto.ch/typo3-ext-gridgallery)
- **Gitlab Repository**: [gitlab.com/lavitto/typo3-gridgallery](https://gitlab.com/lavitto/typo3-gridgallery)
- **TYPO3 Extension Repository**: [extensions.typo3.org/extension/gridgallery](https://extensions.typo3.org/extension/gridgallery/)
- **Found an issue?**: [gitlab.com/lavitto/typo3-gridgallery/issues](https://gitlab.com/lavitto/typo3-gridgallery/issues)

## 1. Introduction

### Features

- Based on extbase & fluid
- Simple and fast installation
- No configuration needed
- No user-side image-manipulation needed
- Fully responsive
- Support for high resolution screens

### Screenshots

#### Smartphone

![Smartphone Example](https://cdn.lavitto.ch/typo3/lavitto/typo3-gridgallery/gridgallery-fe-xs_tmb.png)
- [Full Size Screenshot](https://cdn.lavitto.ch/typo3/lavitto/typo3-gridgallery/gridgallery-fe-xs.png)

#### Tablet

![Tablet Example](https://cdn.lavitto.ch/typo3/lavitto/typo3-gridgallery/gridgallery-fe-sm_tmb.png)
- [Full Size Screenshot](https://cdn.lavitto.ch/typo3/lavitto/typo3-gridgallery/gridgallery-fe-sm.png)

#### Desktop

![Desktop Example](https://cdn.lavitto.ch/typo3/lavitto/typo3-gridgallery/gridgallery-fe-md_tmb.png)
- [Full Size Screenshot](https://cdn.lavitto.ch/typo3/lavitto/typo3-gridgallery/gridgallery-fe-md.png)

## 2. Installation

### Installation using Composer

The recommended way to install the extension is by using [Composer](https://getcomposer.org/). In your Composer based 
TYPO3 project root, just do `composer req lavitto/typo3-gridgallery`.

### Installation from TYPO3 Extension Repository (TER)

Download and install the extension `gridgallery` with the extension manager module.

## 3. Minimal setup

1)  Include the static TypoScript of the extension.
2)  Create a grid gallery content element on a page

## 4. Administration

### Create content element

1)  Create a new content element and select "Grid Gallery"

![Create content element](https://cdn.lavitto.ch/typo3/lavitto/typo3-gridgallery/gridgallery-be1_tmb.png)
- [Full Size Screenshot](https://cdn.lavitto.ch/typo3/lavitto/typo3-gridgallery/gridgallery-be1.png) 

### Add images and set options

1) Simple upload or add media files (currently only images are supported!)
2) Enable/disable click-enlarge function
3) Override [default row height](#5-configuration)
4) Override [default margins](#5-configuration)

![Add images and set options](https://cdn.lavitto.ch/typo3/lavitto/typo3-gridgallery/gridgallery-be2_tmb.png)
- [Full Size Screenshot](https://cdn.lavitto.ch/typo3/lavitto/typo3-gridgallery/gridgallery-be2.png)

## 5. Configuration

### Constants

This default properties can be changed by **Constant Editor**:

| Property            | Description                                    | Type      | Default value   |
| ------------------  | ---------------------------------------------- | --------- | --------------- |
| defaultRowHeight    | Default height of each gallery row in pixels   | integer   | 150             |
| defaultRenderFactor | Default render factor of gallery images. Rendered image height = defaultRowHeight * defaultRenderFactor | float   | 1.5 |
| defaultMargins      | Default margin between the images in pixels    | integer   | 2               |
| defaultCaptions     | Enable to show captions by default             | boolean   | true            |
| defaultRandomize    | Enable to randomize the image-order by default | boolean   | false           |
| defaultLastRow      | Default value to handle the last row           | options   | nojustify       |
| enableJquery        | Includes jQuery to the page                    | boolean   | false           |

## 6. Contribute

Please create an issue at https://gitlab.com/lavitto/typo3-gridgallery/issues.

**Please use GitLab only for bug-reports or feature-requests. For support use the TYPO3 community channels or contact us by email.**

## 7. Support

If you need private or personal support, contact us by email on [info@lavitto.ch](mailto:info@lavitto.ch). 

**Be aware that this support might not be free!**
